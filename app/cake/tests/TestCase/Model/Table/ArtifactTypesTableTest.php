<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArtifactTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArtifactTypesTable Test Case
 */
class ArtifactTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ArtifactTypesTable
     */
    public $ArtifactTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.artifact_types',
        'app.artifacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArtifactTypes') ? [] : ['className' => ArtifactTypesTable::class];
        $this->ArtifactTypes = TableRegistry::getTableLocator()->get('ArtifactTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArtifactTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
