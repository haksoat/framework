<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CdlNote $cdlNote
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('View Cdl Note') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Year') ?></th>
                    <td><?= h($cdlNote->year) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Author') ?></th>
                    <td><?= $cdlNote->has('author') ? $this->Html->link($cdlNote->author->id, ['controller' => 'Authors', 'action' => 'view', $cdlNote->author->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Preprint State') ?></th>
                    <td><?= h($cdlNote->preprint_state) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Online') ?></th>
                    <td><?= h($cdlNote->online) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($cdlNote->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Number') ?></th>
                    <td><?= $this->Number->format($cdlNote->number) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Uploaded') ?></th>
                    <td><?= h($cdlNote->uploaded) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Preprint') ?></th>
                    <td><?= h($cdlNote->preprint) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Archival') ?></th>
                    <td><?= h($cdlNote->archival) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Title') ?></th>
                    <td><?= $this->Text->autoParagraph(h($cdlNote->title)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Text') ?></th>
                    <td><?= $this->Text->autoParagraph(h($cdlNote->text)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Footnotes') ?></th>
                    <td><?= $this->Text->autoParagraph(h($cdlNote->footnotes)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Bibliography') ?></th>
                    <td><?= $this->Text->autoParagraph(h($cdlNote->bibliography)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Edit Cdl Note'), ['action' => 'edit', $cdlNote->id], ['class' => 'btn-action']) ?>
        <?= $this->Form->postLink(__('Delete Cdl Note'), ['action' => 'delete', $cdlNote->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cdlNote->id), 'class' => 'btn-action']) ?>
        <?= $this->Html->link(__('List Cdl Notes'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Cdl Note'), ['action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['controller' => 'Authors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Author'), ['controller' => 'Authors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>



