<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsMaterial $artifactsMaterial
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($artifactsMaterial) ?>
            <legend class="capital-heading"><?= __('Add Artifacts Material') ?></legend>
            <?php
                echo $this->Form->control('artifact_id', ['options' => $artifacts]);
                echo $this->Form->control('material_id', ['options' => $materials]);
                echo $this->Form->control('is_material_uncertain');
                echo $this->Form->control('material_color_id', ['options' => $materialColors, 'empty' => true]);
                echo $this->Form->control('material_aspect_id', ['options' => $materialAspects, 'empty' => true]);
            ?>

            <?= $this->Form->submit() ?>
        <?= $this->Form->end() ?>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Artifacts Materials'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Materials'), ['controller' => 'Materials', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Material'), ['controller' => 'Materials', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Material Colors'), ['controller' => 'MaterialColors', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Material Color'), ['controller' => 'MaterialColors', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Material Aspects'), ['controller' => 'MaterialAspects', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Material Aspect'), ['controller' => 'MaterialAspects', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
