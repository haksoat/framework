<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Publications Model
 *
 * @property \App\Model\Table\EntryTypesTable|\Cake\ORM\Association\BelongsTo $EntryTypes
 * @property \App\Model\Table\JournalsTable|\Cake\ORM\Association\BelongsTo $Journals
 * @property \App\Model\Table\AbbreviationsTable|\Cake\ORM\Association\BelongsTo $Abbreviations
 * @property \App\Model\Table\ArtifactsTable|\Cake\ORM\Association\BelongsToMany $Artifacts
 * @property \App\Model\Table\AuthorsTable|\Cake\ORM\Association\BelongsToMany $Authors
 *
 * @method \App\Model\Entity\Publication get($primaryKey, $options = [])
 * @method \App\Model\Entity\Publication newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Publication[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Publication|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Publication|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Publication patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Publication[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Publication findOrCreate($search, callable $callback = null, $options = [])
 */
class PublicationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('publications');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->belongsTo('EntryTypes', [
            'foreignKey' => 'entry_type_id'
        ]);
        $this->belongsTo('Journals', [
            'foreignKey' => 'journal_id'
        ]);
        $this->belongsTo('Abbreviations', [
            'foreignKey' => 'abbreviation_id'
        ]);
        $this->belongsToMany('Artifacts', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'artifact_id',
            'joinTable' => 'artifacts_publications'
        ]);
        $this->belongsToMany('Authors', [
            'foreignKey' => 'publication_id',
            'targetForeignKey' => 'author_id',
            'joinTable' => 'authors_publications'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('bibtexkey')
            ->allowEmpty('bibtexkey');

        $validator
            ->scalar('year')
            ->maxLength('year', 20)
            ->allowEmpty('year');

        $validator
            ->scalar('address')
            ->maxLength('address', 45)
            ->allowEmpty('address');

        $validator
            ->scalar('annote')
            ->maxLength('annote', 45)
            ->allowEmpty('annote');

        $validator
            ->scalar('book_title')
            ->maxLength('book_title', 255)
            ->allowEmpty('book_title');

        $validator
            ->scalar('chapter')
            ->maxLength('chapter', 100)
            ->allowEmpty('chapter');

        $validator
            ->scalar('crossref')
            ->maxLength('crossref', 45)
            ->allowEmpty('crossref');

        $validator
            ->scalar('edition')
            ->maxLength('edition', 45)
            ->allowEmpty('edition');

        $validator
            ->scalar('editor')
            ->maxLength('editor', 100)
            ->allowEmpty('editor');

        $validator
            ->scalar('how_published')
            ->maxLength('how_published', 255)
            ->allowEmpty('how_published');

        $validator
            ->scalar('institution')
            ->maxLength('institution', 45)
            ->allowEmpty('institution');

        $validator
            ->scalar('month')
            ->maxLength('month', 45)
            ->allowEmpty('month');

        $validator
            ->scalar('note')
            ->maxLength('note', 45)
            ->allowEmpty('note');

        $validator
            ->scalar('number')
            ->maxLength('number', 100)
            ->allowEmpty('number');

        $validator
            ->scalar('organization')
            ->maxLength('organization', 45)
            ->allowEmpty('organization');

        $validator
            ->scalar('pages')
            ->maxLength('pages', 45)
            ->allowEmpty('pages');

        $validator
            ->scalar('publisher')
            ->maxLength('publisher', 100)
            ->allowEmpty('publisher');

        $validator
            ->scalar('school')
            ->maxLength('school', 80)
            ->allowEmpty('school');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmpty('title');

        $validator
            ->scalar('volume')
            ->maxLength('volume', 50)
            ->allowEmpty('volume');

        $validator
            ->scalar('publication_history')
            ->allowEmpty('publication_history');

        $validator
            ->scalar('series')
            ->maxLength('series', 100)
            ->allowEmpty('series');

        $validator
            ->integer('oclc')
            ->allowEmpty('oclc');

        $validator
            ->scalar('designation')
            ->allowEmpty('designation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['entry_type_id'], 'EntryTypes'));
        $rules->add($rules->existsIn(['journal_id'], 'Journals'));
        $rules->add($rules->existsIn(['abbreviation_id'], 'Abbreviations'));

        return $rules;
    }
}
