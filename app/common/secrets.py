"""
    Python 2/3 Secrets Library -- Version 0.9.0

    USAGE:
        import secrets
        secrets.getSecrets('secretID', ...) => returns dict
        secrets.getLocalSecrets('secretID', ...) => returns dict
        secrets.getRemoteSecrets('secretID', ...) => returns dict
"""

from __future__ import print_function

import glob
import json
import os
import re

try:
    # Python 2
    from urllib import quote_plus
    from urllib2 import HTTPError, Request, URLError, urlopen
    string_types = basestring  # noqa: F821
except ImportError:
    # Python 3
    from urllib.error import HTTPError, URLError
    from urllib.parse import quote_plus
    from urllib.request import Request, urlopen
    string_types = (str, bytes)


#
# Module constants/variables
#

__all__ = ['getSecrets', 'getLocalSecrets', 'getRemoteSecrets']
_SECRETS_URI = os.environ.get('SECRETS_URI', 'http://localhost/secrets?')
_SECRETS_LOCAL = os.environ.get('SECRETS_LOCAL', '/var/secrets/*.json')


# Determine environment and secret source on load.
if re.search(r'\b(?:prod|staging|remote)', os.environ.get('CDLI_ENV', ''),
             re.IGNORECASE):
    _SECRET_SRC = 'remote'
else:
    _SECRET_SRC = 'local'


#
# Module Methods
#

def getSecrets(*ids):
    """Get a dict of secrets from somewhere."""
    if _SECRET_SRC == 'remote':
        return getRemoteSecrets(*ids)
    else:
        return getLocalSecrets(*ids)


def getLocalSecrets(*ids):
    """Get local secrets."""
    secrets = {}
    results = {}
    for secretFile in glob.glob(_SECRETS_LOCAL):
        try:
            file = open(secretFile)
            data = json.loads(file.read())
            if isinstance(data, dict):
                secrets.update(data)
            else:
                raise ValueError()
        except IOError:
            results['_error'] = 'file read error'
        except ValueError:
            results['_error'] = 'JSON error'
    for id in ids:
        results[id] = secrets.get(id, {'_error': 'not found'})
    return results


def getRemoteSecrets(*ids):
    """Get secrets from an HTTP endpoint like SecretsD."""
    uri = _SECRETS_URI + _parseIdsToQuery(ids)
    try:
        req = Request(uri)
        res = urlopen(req)
        data = json.loads(res.read())
        if isinstance(data, dict):
            return data
        else:
            raise ValueError()
    except HTTPError as e:
        return {'_error': e.code}
    except URLError as e:
        return {'_error': '%s' % e.reason.strerror}
    except ValueError:
        return {'_error': 'JSON error'}


def _parseIdsToQuery(ids):
    """Parse a sequence of IDs into a query string."""
    try:
        return '&'.join(quote_plus(str(id)) for id in ids)
    except TypeError:
        return quote_plus(str(ids))
